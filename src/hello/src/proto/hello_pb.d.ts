// package:
// file: src/proto/hello.proto

import * as jspb from 'google-protobuf';

// eslint-disable-next-line import/export
export class HelloRequest extends jspb.Message {
  getName(): string;

  setName(value: string): void;

  serializeBinary(): Uint8Array;

  toObject(includeInstance?: boolean): HelloRequest.AsObject;

  static toObject(includeInstance: boolean, msg: HelloRequest): HelloRequest.AsObject;

  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};

  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};

  static serializeBinaryToWriter(message: HelloRequest, writer: jspb.BinaryWriter): void;

  static deserializeBinary(bytes: Uint8Array): HelloRequest;

  // eslint-disable-next-line max-len
  static deserializeBinaryFromReader(message: HelloRequest, reader: jspb.BinaryReader): HelloRequest;
}

// eslint-disable-next-line import/export,no-redeclare
export namespace HelloRequest {
  export type AsObject = {
    name: string,
  }
}

// eslint-disable-next-line import/export
export class HelloReply extends jspb.Message {
  getMessage(): string;

  setMessage(value: string): void;

  serializeBinary(): Uint8Array;

  toObject(includeInstance?: boolean): HelloReply.AsObject;

  static toObject(includeInstance: boolean, msg: HelloReply): HelloReply.AsObject;

  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};

  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};

  static serializeBinaryToWriter(message: HelloReply, writer: jspb.BinaryWriter): void;

  static deserializeBinary(bytes: Uint8Array): HelloReply;

  static deserializeBinaryFromReader(message: HelloReply, reader: jspb.BinaryReader): HelloReply;
}

// eslint-disable-next-line import/export,no-redeclare
export namespace HelloReply {
  export type AsObject = {
    message: string,
  }
}
