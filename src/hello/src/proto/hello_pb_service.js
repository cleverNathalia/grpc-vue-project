// package:
// file: src/proto/hello.proto

const { grpc } = require('@improbable-eng/grpc-web');
// eslint-disable-next-line camelcase
const src_proto_hello_pb = require('../../src/proto/hello_pb');

const HelloService = (function () {
  // eslint-disable-next-line no-shadow
  function HelloService() {}
  HelloService.serviceName = 'HelloService';
  return HelloService;
}());

HelloService.Hello = {
  methodName: 'Hello',
  service: HelloService,
  requestStream: false,
  responseStream: false,
  requestType: src_proto_hello_pb.HelloRequest,
  responseType: src_proto_hello_pb.HelloReply,
};

exports.HelloService = HelloService;

function HelloServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

HelloServiceClient.prototype.hello = function hello(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    // eslint-disable-next-line no-param-reassign,prefer-destructuring,prefer-rest-params
    callback = arguments[1];
  }
  const client = grpc.unary(HelloService.Hello, {
    request: requestMessage,
    host: this.serviceHost,
    metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd(response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          const err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    },
  });
  return {
    cancel() {
      // eslint-disable-next-line no-param-reassign
      callback = null;
      client.close();
    },
  };
};

exports.HelloServiceClient = HelloServiceClient;
